# Same Page Preview

## Description

Provide a side-by-side preview of the current page in the same window.

## Requirements

In Drupal 10.3 and above this module has no additional dependencies.

In Drupal 10.1 and 10.2 Same Page Preview requires the Single Directory Components (SDC) experimental module. It must be enabled manually.

## Developer Tooling

### Linting

Prerequisites:

- Install nodejs
- Run `npm install` in this directory.

Scripts:

- `npm run eslint` - Lint all files in the project handled by eslint.
- `npm run eslint:fix` - Lint all files in the project handled by eslint and fix all possible issues.
